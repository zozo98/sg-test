import java.util.*;

public class Test {

   public static boolean lookForTarget(int[] list, int target) {
      HashMap<Integer, Integer> valMap = new HashMap<Integer, Integer>(); 
      for (int valTemp : list){
         valMap.put(valTemp, 1);
      }
      for (int valTemp : list){
         int possibleKey = target - valTemp;
         if (valMap.containsKey(possibleKey)) {
            return true;
         }
      }
      return false;
   }

   public static void testCommand(int[] list, int target, boolean expectingResult) {
      boolean result = lookForTarget(list, target);
      System.out.print("List : [ ");
      for (int valTemp : list){
         System.out.print(valTemp + " ");   
      }
      System.out.println("]");
      System.out.println("target : " + target);
      System.out.println("Result expected : " + expectingResult);
      System.out.println("Result obtainted : " + result);
   }

   public static void main(String[] args) {
      int[] list = {1, 3, 2, 3, 5, 9, 12, 21, 8};
      System.out.println("Test 1");
      testCommand(list, 5, true);
      System.out.println("Test 2");
      testCommand(list, 24, true);
      System.out.println("Test 3");
      testCommand(list, 43, false);
      System.out.println("Test 4");
      testCommand(list, 33, true);
      System.out.println("Test 5");
      testCommand(list, 122, false);
   }
}